/**
 * Calculating UV coordinates for extrude geometry:
 * @see https://stackoverflow.com/questions/20774648/three-js-generate-uv-coordinate
 * @see http://paulyg.f2s.com/uv.htm
 */

import * as THREE from '/scripts/three/build/three.module.js';
import { createBuilding } from './building.js';

const SHADOW_MAP_WIDTH = 2048;
const SHADOW_MAP_HEIGHT = 1024;
const FLOOR = -250;
const NEAR = 10;
const FAR = 3000;
let camera, scene, renderer;
let SCREEN_WIDTH = window.innerWidth;
let SCREEN_HEIGHT = window.innerHeight;
let container;
let light;
let rect;
let cube;

export function createWorld(containerEl) {
  setup(containerEl);
  createGround();
  // createCubes();
  // createWalls();
  // createExtrudes();
  // createShape();
  createBuildings();
  animate();
}

function setup(containerEl) {
  rect = containerEl.getBoundingClientRect();

  // CAMERA
  camera = new THREE.PerspectiveCamera( 23, SCREEN_WIDTH / SCREEN_HEIGHT, NEAR, FAR );
  camera.position.set( 700, 50, 1900 );

  // SCENE
  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0x59472b );
  // scene.fog = new THREE.Fog( 0x59472b, 1000, FAR );

  // LIGHTS
  var ambient = new THREE.AmbientLight( 0x444444 );
  scene.add( ambient );

  light = new THREE.SpotLight( 0xffffff, 1, 0, Math.PI / 2 );
  light.position.set( 0, 1500, 1000 );
  light.target.position.set( 0, 0, 0 );

  light.castShadow = true;

  light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
  light.shadow.bias = 0.0001;

  light.shadow.mapSize.width = SHADOW_MAP_WIDTH;
  light.shadow.mapSize.height = SHADOW_MAP_HEIGHT;
  scene.add(light);

  // RENDERER
  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
  containerEl.appendChild( renderer.domElement );

  renderer.autoClear = false;
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFShadowMap;

  window.addEventListener( 'resize', onWindowResize, false );
}

function onWindowResize() {
  SCREEN_WIDTH = window.innerWidth;
  SCREEN_HEIGHT = window.innerHeight;

  camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
  camera.updateProjectionMatrix();

  renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
}

function createGround( ) {
  // GROUND
  var geometry = new THREE.PlaneBufferGeometry(100, 100);
  var planeMaterial = new THREE.MeshPhongMaterial( { color: 0xffdd99 } );

  var ground = new THREE.Mesh(geometry, planeMaterial);
  ground.position.set( 0, FLOOR, 0 );
  ground.rotation.x = - Math.PI / 2;
  ground.scale.set( 100, 100, 100 );
  ground.castShadow = false;
  ground.receiveShadow = true;
  scene.add( ground );
}

function createCubes( ) {
  var planeMaterial = new THREE.MeshPhongMaterial( { color: 0xffdd99 } );
  
  var mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 1500, 220, 150 ), planeMaterial );
  mesh.position.y = FLOOR - 50;
  mesh.position.z = 20;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );

  var mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 1600, 170, 250 ), planeMaterial );
  mesh.position.y = FLOOR - 50;
  mesh.position.z = 20;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );

  var mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 100, 100, 100 ), planeMaterial );
  mesh.position.x = camera.position.x;
  mesh.position.y = FLOOR + 250;
  mesh.position.z = 100;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );

  cube = mesh;
}

function createBuildings() {
  var shape = new THREE.Shape();
  shape.moveTo(-200, FLOOR);
  shape.lineTo(-200, 350);
  shape.lineTo(-20, 400);
  shape.lineTo(200, 300);
  shape.lineTo(200, FLOOR);
  shape.lineTo(-200, FLOOR);

  createBuilding(shape).then(mesh => {
    mesh.position.set(camera.position.x, 0, -100);
    scene.add(mesh);
  });
}

function animate() {
  requestAnimationFrame(animate);
  // cube.rotation.x += 0.01;
  // cube.rotation.y += 0.01;
  renderer.clear();
  renderer.render( scene, camera );
}

// function createExtrude() {
//   var shape = new THREE.Shape();
//   shape.moveTo(-200, FLOOR);
//   shape.lineTo(-200, 350);
//   shape.lineTo(-20, 400);
//   shape.lineTo(200, 300);
//   shape.lineTo(200, FLOOR);
//   shape.lineTo(-200, FLOOR);

//   var extrudeSettings = {
//     steps: 2,
//     depth: 16,
//     bevelEnabled: true,
//     bevelThickness: 1,
//     bevelSize: 1,
//     bevelSegments: 1
//   };

//   var geometry = new THREE.ExtrudeGeometry( shape, extrudeSettings );
//   var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
//   var mesh = new THREE.Mesh(geometry, material);

//   geometry.computeBoundingBox();
//   var max = geometry.boundingBox.max,
//       min = geometry.boundingBox.min;
//   var offset = new THREE.Vector2(0 - min.x, 0 - min.y);
//   var range = new THREE.Vector2(max.x - min.x, max.y - min.y);
//   var faces = geometry.faces;

//   geometry.faceVertexUvs[0] = [];

//   for (var i = 0; i < faces.length ; i++) {

//       var v1 = geometry.vertices[faces[i].a], 
//           v2 = geometry.vertices[faces[i].b], 
//           v3 = geometry.vertices[faces[i].c];

//       geometry.faceVertexUvs[0].push([
//           new THREE.Vector2((v1.x + offset.x)/range.x ,(v1.y + offset.y)/range.y),
//           new THREE.Vector2((v2.x + offset.x)/range.x ,(v2.y + offset.y)/range.y),
//           new THREE.Vector2((v3.x + offset.x)/range.x ,(v3.y + offset.y)/range.y)
//       ]);
//   }
//   geometry.uvsNeedUpdate = true;

//   console.log(geometry);

//   // new THREE.TextureLoader().load('../img/plaza_lavapies_2048x1024.png', function(texture) {
//   new THREE.TextureLoader().load('../img/madrid_7448.png', function(texture) {
//     const meshHeight = 650; // 400 - FLOOR;
//     const meshWidth = 400;
//     const ratioS = texture.image.width / meshWidth;
//     const ratioT = texture.image.height / meshHeight;

//     let repeatS;
//     let repeatT;
//     if (ratioS < ratioT) {
//       repeatS = 1;
//       repeatT = (meshHeight / meshWidth) / (texture.image.height / texture.image.width);
//     } else {
//       repeatS = (meshWidth / meshHeight) / (texture.image.width / texture.image.height);
//       repeatT = 1;
//     }
//     texture.repeat.set(repeatS, repeatT);
//     console.log('repeatS & T', repeatS, repeatT);

//     const offsetX = 400;
//     const offsetY = 0;
//     if (!isNaN(offsetX) && !isNaN(offsetY)) {
//       texture.offset = new THREE.Vector2(
//         offsetX / texture.image.width,
//         offsetY / texture.image.height,
//       );
//     }

//     const material = new THREE.MeshPhongMaterial({map: texture});
//     var mesh = new THREE.Mesh(geometry, material);
//     mesh.castShadow = true;
//     mesh.receiveShadow = true;
//     mesh.position.set(camera.position.x, 0, -100);
//     scene.add(mesh);
//   });
// }

// function createWalls( ) {
//   // walls
//   const scale = 1;
//   const offsetX = 0;
//   const offsetY = 0;
//   for (let i = 0; i < 5; i++) {
//   new THREE.TextureLoader().load('../img/plaza_lavapies_2048x1024.png', function(texture) {
//       const meshHeight = 500 + (Math.random() * 300);
//       const meshWidth = 100 + (Math.random() * 400);
//       const ratioS = texture.image.width / meshWidth;
//       const ratioT = texture.image.height / meshHeight;

//       let repeatS;
//       let repeatT;
//       if (ratioS < ratioT) {
//         repeatS = 1;
//         repeatT = (meshHeight / meshWidth) / (texture.image.height / texture.image.width);
//       } else {
//         repeatS = (meshWidth / meshHeight) / (texture.image.width / texture.image.height);
//         repeatT = 1;
//       }
//       console.log('repeatS & T', repeatS, repeatT);

//       // apply scaling as long as it scales up
//       if (scale && scale > 1) {
//         repeatS /= scale;
//         repeatT /= scale;
//       }

//       if (offsetX && offsetY) {
//         texture.offset = new THREE.Vector2(
//           offsetX / texture.image.width,
//           offsetY / texture.image.height,
//         );
//       }

//       console.log('scaled repeatS & T', repeatS, repeatT);

//       texture.repeat.set(repeatS, repeatT);

//       const wallMaterial = new THREE.MeshPhongMaterial( { map: texture } );
//       let wall = new THREE.Mesh(new THREE.BoxBufferGeometry(meshWidth, meshHeight, 10), wallMaterial);
//       wall.position.x = camera.position.x + 600 - (Math.random() * 1200);
//       wall.position.y = FLOOR + (meshHeight / 2);
//       wall.position.z = -100 - (Math.random() * 800);
//       wall.castShadow = true;
//       wall.receiveShadow = true;
//       scene.add(wall);
//     });
//   }

  // const meshHeight = 64 * 6;
  // const meshWidth = 64 * 9;
  // const scale = 4;
  // const offsetX = 64;
  // const offsetY = 64;

  // new THREE.TextureLoader().load('../img/reference_grid_768x512.png', function(texture) {

  //   texture.wrapS = THREE.RepeatWrapping;
  //   texture.wrapT = THREE.RepeatWrapping;
  //   const ratioS = texture.image.width / meshWidth;
  //   const ratioT = texture.image.height / meshHeight;
  //   console.log('ratioS & T', ratioS, ratioT);

  //   // display cover, smallest size to cover the mesh without repeating
  //   let repeatS;
  //   let repeatT;
  //   if (ratioS < ratioT) {
  //     repeatS = 1;
  //     repeatT = (meshHeight / meshWidth) / (texture.image.height / texture.image.width);
  //   } else {
  //     repeatS = (meshWidth / meshHeight) / (texture.image.width / texture.image.height);
  //     repeatT = 1;
  //   }
  //   console.log('repeatS & T', repeatS, repeatT);

  //   // apply scaling as long as it scales up
  //   if (scale && scale > 1) {
  //     repeatS /= scale;
  //     repeatT /= scale;
  //   }

  //   texture.offset = new THREE.Vector2(
  //     offsetX / texture.image.width,
  //     offsetY / texture.image.height,
  //   );

  //   console.log('scaled repeatS & T', repeatS, repeatT);

  //   texture.repeat.set(repeatS, repeatT);

  //   const wallMaterial = new THREE.MeshPhongMaterial( { map: texture } );
  //   const wall = new THREE.Mesh(new THREE.BoxBufferGeometry(meshWidth, meshHeight, 10), wallMaterial);
  //   wall.position.x = camera.position.x;
  //   wall.position.y = FLOOR + (meshHeight / 2);
  //   wall.position.z = 100;
  //   scene.add(wall);
  // });
// }

// function createShape() {
//   const shape = new THREE.Shape();
//   shape.moveTo(-200, FLOOR);
//   shape.lineTo(-200, 350);
//   shape.lineTo(-20, 400);
//   shape.lineTo(200, 300);
//   shape.lineTo(200, FLOOR);
//   shape.lineTo(-200, FLOOR);

//   // var x = 0;
//   // var y = 0;
//   // var shape = new THREE.Shape();
//   // shape.moveTo( x, y );
//   // shape.quadraticCurveTo( x + 50, y - 80, x + 90, y - 10 );
//   // shape.quadraticCurveTo( x + 100, y - 10, x + 115, y - 40 );
//   // shape.quadraticCurveTo( x + 115, y, x + 115, y + 40 );
//   // shape.quadraticCurveTo( x + 100, y + 10, x + 90, y + 10 );
//   // shape.quadraticCurveTo( x + 50, y + 80, x, y );

//   const extrudeSettings = { 
//     depth: 8, 
//     bevelEnabled: true, 
//     bevelSegments: 2, 
//     steps: 2, 
//     bevelSize: 1, 
//     bevelThickness: 1 
//   };

//   new THREE.TextureLoader().load('../img/madrid_7448.png', function(texture) {
//     texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
//     texture.repeat.set( 0.0008, 0.0008 );
    
//     var geometry = new THREE.ShapeBufferGeometry(shape);

//     var mesh = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({side: THREE.DoubleSide, map: texture}));
//     mesh.position.set(camera.position.x, 0, -100);
//     mesh.rotation.set(0, 0, 0);
//     mesh.scale.set(1, 1, 1);
//     mesh.castShadow = true;
//     mesh.receiveShadow = true;
//     scene.add(mesh);
//   });
// }
