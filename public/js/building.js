import * as THREE from '/scripts/three/build/three.module.js';

const extrudeSettings = {
  steps: 2,
  depth: 16,
  bevelEnabled: true,
  bevelThickness: 1,
  bevelSize: 1,
  bevelSegments: 1
};

/**
 * 
 * @param {*} shape 
 */
function createGeometry(shape) {
  const geometry = new THREE.ExtrudeGeometry( shape, extrudeSettings );
  geometry.computeBoundingBox();

  const max = geometry.boundingBox.max;
  const min = geometry.boundingBox.min;
  const offset = new THREE.Vector2(0 - min.x, 0 - min.y);
  const range = new THREE.Vector2(max.x - min.x, max.y - min.y);
  const faces = geometry.faces;

  geometry.faceVertexUvs[0] = [];

  for (let i = 0; i < faces.length ; i++) {
    const v1 = geometry.vertices[faces[i].a];
    const v2 = geometry.vertices[faces[i].b]; 
    const v3 = geometry.vertices[faces[i].c];

    geometry.faceVertexUvs[0].push([
        new THREE.Vector2((v1.x + offset.x)/range.x ,(v1.y + offset.y)/range.y),
        new THREE.Vector2((v2.x + offset.x)/range.x ,(v2.y + offset.y)/range.y),
        new THREE.Vector2((v3.x + offset.x)/range.x ,(v3.y + offset.y)/range.y)
    ]);
  }

  geometry.uvsNeedUpdate = true;

  return geometry;
}

/**
 * 
 */
function createMaterial() {
  return new Promise(resolve => {
    new THREE.TextureLoader().load('../img/madrid_7448.png', function(texture) {
      const meshHeight = 650; // 400 - FLOOR;
      const meshWidth = 400;
      const ratioS = texture.image.width / meshWidth;
      const ratioT = texture.image.height / meshHeight;

      let repeatS;
      let repeatT;
      if (ratioS < ratioT) {
        repeatS = 1;
        repeatT = (meshHeight / meshWidth) / (texture.image.height / texture.image.width);
      } else {
        repeatS = (meshWidth / meshHeight) / (texture.image.width / texture.image.height);
        repeatT = 1;
      }
      texture.repeat.set(repeatS, repeatT);
      console.log('repeatS & T', repeatS, repeatT);

      const offsetX = 400;
      const offsetY = 0;
      if (!isNaN(offsetX) && !isNaN(offsetY)) {
        texture.offset = new THREE.Vector2(
          offsetX / texture.image.width,
          offsetY / texture.image.height,
        );
      }

      const material = new THREE.MeshPhongMaterial({map: texture});

      resolve(material);
    });
  });
}

/**
 * 
 * @param {*} geometry 
 * @param {*} material 
 */
function createMesh(geometry, material) {
  const mesh = new THREE.Mesh(geometry, material);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  return mesh;
}

/**
 * 
 * @param {*} shape 
 */
export function createBuilding(shape) {
  return new Promise(resolve => {
    createMaterial().then(material => {
      const geometry = createGeometry(shape);
      const mesh = createMesh(geometry, material);
      resolve(mesh);
    });
  });
}