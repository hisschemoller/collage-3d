# Collage 3D

This project creates photo collages in 3D.

It's also to test a new way to set up a vanilla JS project
without a framework. For quick prototyping.
Using this tutorial:
https://www.sitepoint.com/single-page-app-without-framework/

## Concepts

Image textures are applied to sides of boxes.

By default the texture is scaled so that it exactly covers the side of a box. 

That scale is considered the default scale, or scale value 1, for that texure on that box side.